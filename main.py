from kubernetes import client, config
from openshift.dynamic import DynamicClient

import warnings
import time
import sys
from com.lguplus.common import *


OCP_VERSION = sys.argv[1]

##print("OCP_VERSION=" + OCP_VERSION)


warnings.filterwarnings("ignore")

k8s_client = config.new_client_from_config()
dyn_client = DynamicClient(k8s_client)

loop = []

    
## Report Header
print("PROJECT\tDISPLAYNAME\tCREATIONTIME\tEGRESSIP\tHOSTSUBNET\tRESOURCEQUOTA\t\t\t\t\tDEPLOYMENT\t\t\t\t\t\t\t\t\tLIMITRANGE\t\t\t\t\t\t\t\t\t\t\t\t\tNETWORK\t\t\t\t\t\t\t\tVOLUME\t\t\t\t\t\t\t\t")
print("\t\t\t\t\tNAME\tCPU\t\tMEMORY\t\tTYPE\tNAME\tUPDATESTRATEGE\tREPLICA\tCONTAINER\tCPU\t\tMEMORY\t\tNAME\tPOD\t\t\t\tCONTAINER\t\t\t\tDEFAULT\t\t\t\tROUTE\t\t\t\tINGRESS\t\tEXTERNALIPS\t\tPVC\t\t\tPV\t\t\t\t\t")
print("\t\t\t\t\t\tREQUEST\tLIMIT\tREQUEST\tLIMIT\t\t\t\t\t\tREQUEST\tLIMIT\tREQUEST\tLIMIT\t\tMAX_CPU\tMAX_MEMORY\tMIN_CPU\tMIM_MEMORY\tMAX_CPU\tMAX_MEMORY\tMIN_CPU\tMIN_MAMOEY\tCPU\tMEMORY\tREQUEST_CPU\tREQUEST_MEMORY\tNAME\tURL\tTERMINATION\tDEST\tNAME\tURL\tNAME\tEXTERNAL_IPS\tNAME\tSTATUS\tSIZE\tNAME\tNFS.SPEC\tSERVER\tPATH\tCLAIMPOLICY\t")

## Get PersistentVolume
v1_persistentvolumes = dyn_client.resources.get(api_version='v1', kind='PersistentVolume')
persistentvolum_list = v1_persistentvolumes.get()

# Get Hostsubnet
v1_hostsubnets = dyn_client.resources.get(api_version='network.openshift.io/v1', kind='HostSubnet')
hostsubnet_list = v1_hostsubnets.get()

# Get NetNamespace
v1_netnamespaces = dyn_client.resources.get(api_version='network.openshift.io/v1', kind='NetNamespace')
netnamespace_list = v1_netnamespaces.get()

## Retreive Data from K8S for each project
v1_projects = dyn_client.resources.get(api_version='project.openshift.io/v1', kind='Project')
project_list = v1_projects.get()

for project in project_list.items:
    
    if project.metadata.name in EXCEPT_PROJECT:
        continue

    project_output = list()
    
    displayname = NoneToBlank(project.metadata.annotations.get("openshift.io/display-name"))
    
    project_output.append(project.metadata.name + "\t" + displayname + "\t" + project.metadata.get("creationTimestamp") + "\t") 
    
    netnamespace = FindNetNamespaceByName(netnamespace_list, project.metadata.name)
    netnamespace_output = list()
    
    if netnamespace is not None:
        netnamespace_egressips_list = netnamespace.egressIPs
        if netnamespace_egressips_list is not None:
            for netnamespace_egressips in netnamespace_egressips_list:
                netnamespace_output.append(netnamespace_egressips + "\t" + FindHostsubnetbyEgressIP(hostsubnet_list, netnamespace_egressips) + "\t")
    else:
        netnamespace_output.append("\t\t")
    
    ## Get ResourceQuota
    v1_resourcequotas = dyn_client.resources.get(api_version='v1', kind='ResourceQuota')
    resourcequota_list = v1_resourcequotas.get(namespace=project.metadata.name)
    resourcequota_output = list()
    
    for resourcequota in resourcequota_list.items:
        resourcequota_output.append(resourcequota.metadata.name + "\t" + MeasureToNum(resourcequota.spec.hard.get("requests.cpu")) + "\t" + MeasureToNum(resourcequota.spec.hard.get("limits.cpu")) + "\t" + MeasureToNum(resourcequota.spec.hard.get("requests.memory")) + "\t" + MeasureToNum(resourcequota.spec.hard.get("limits.memory")) + "\t")


    deployment_output = list()
    deployment_container_output = list()
            
    ## Get DeploymentConfig
    v1_deploymentconfigs = dyn_client.resources.get(api_version='apps.openshift.io/v1', kind='DeploymentConfig')
    deploymentconfig_list = v1_deploymentconfigs.get(namespace=project.metadata.name)
    
    for deploymentconfig in deploymentconfig_list.items:
        strategytype = NoneToBlank(deploymentconfig.spec.strategy.get("type"))
            
        deployment_output.append("DeploymentConfig" + "\t" + deploymentconfig.metadata.name + "\t" + strategytype + "\t" + str(deploymentconfig.spec.replicas) + "\t")
           
        deploymentconfig_container_count = 0
        for deploymentconfig_container in deploymentconfig.spec.template.spec.containers:
            limits = deploymentconfig_container.resources.get("limits")
            requests = deploymentconfig_container.resources.get("requests")
            if limits is not None:
                limitscpu = NoneToZero(limits.get("cpu"))
                limitsmemory = NoneToZero(limits.get("memory"))
            else:
                limitscpu = "0"
                limitsmemory = "0"
                
            if requests is not None:
                requestscpu = NoneToZero(requests.get("cpu"))
                requestsmemory = NoneToZero(requests.get("memory"))
            else:
                requestscpu = "0"
                requestsmemory = "0"
                
            deployment_container_output.append(deploymentconfig_container.name + "\t" + MeasureToNum(requestscpu) + "\t" + MeasureToNum(limitscpu) + "\t" + MeasureToNum(requestsmemory) + "\t" + MeasureToNum(limitsmemory) + "\t")
            if deploymentconfig_container_count > 0:
                deployment_output.append("\t\t\t\t")
            deploymentconfig_container_count += 1
            
    
    ## Get Deployment
    v1_deployments = list()
    
    if OCP_VERSION == "3":
        v1_deployments = dyn_client.resources.get(api_version='extensions/v1beta1', kind='Deployment')
    else:
        v1_deployments = dyn_client.resources.get(api_version='apps/v1', kind='Deployment')
    
    deployment_list = v1_deployments.get(namespace=project.metadata.name)

    for deployment in deployment_list.items:
        strategytype = NoneToBlank(deployment.spec.strategy.get("type"))
            
        deployment_output.append("Deployment" + "\t" + deployment.metadata.name + "\t" + strategytype + "\t" + str(deployment.spec.replicas) + "\t")
    
        deployment_container_count = 0
        for deployment_container in deployment.spec.template.spec.containers:
            limits = deployment_container.resources.get("limits")
            requests = deployment_container.resources.get("requests")
            if limits is not None:
                limitscpu = NoneToZero(limits.get("cpu"))
                limitsmemory = NoneToZero(limits.get("memory"))
            else:
                limitscpu = "0"
                limitsmemory = "0"
                
            if requests is not None:
                requestscpu = NoneToZero(requests.get("cpu"))
                requestsmemory = NoneToZero(requests.get("memory"))
            else:
                requestscpu = "0"
                requestsmemory = "0"
                
            deployment_container_output.append(deployment_container.name + "\t" + MeasureToNum(requestscpu) + "\t" + MeasureToNum(limitscpu) + "\t" + MeasureToNum(requestsmemory) + "\t" + MeasureToNum(limitsmemory) + "\t")
            if deployment_container_count > 0:
                deployment_output.append("\t\t\t\t")
            deployment_container_count += 1
        
    ## Get Statefulset
    v1_statefulsets = dyn_client.resources.get(api_version='apps/v1', kind='StatefulSet')
    statefulset_list = v1_statefulsets.get(namespace=project.metadata.name)

    for statefulset in statefulset_list.items:
        strategytype = NoneToBlank(statefulset.spec.updateStrategy.get("type"))
            
        deployment_output.append("StatefulSet" + "\t" + statefulset.metadata.name + "\t" + strategytype + "\t" + str(statefulset.spec.replicas) + "\t")

        statefulset_container_count = 0
        for statefulset_container in statefulset.spec.template.spec.containers:
            limits = statefulset_container.resources.get("limits")
            requests = statefulset_container.resources.get("requests")
            if limits is not None:
                limitscpu = NoneToZero(limits.get("cpu"))
                limitsmemory = NoneToZero(limits.get("memory"))
            else:
                limitscpu = "0"
                limitsmemory = "0"
                
            if requests is not None:
                requestscpu = NoneToZero(requests.get("cpu"))
                requestsmemory = NoneToZero(requests.get("memory"))
            else:
                requestscpu = "0"
                requestsmemory = "0"
                            
            deployment_container_output.append(statefulset_container.name + "\t" + MeasureToNum(requestscpu) + "\t" + MeasureToNum(limitscpu) + "\t" + MeasureToNum(requestsmemory) + "\t" + MeasureToNum(limitsmemory) + "\t")
            if statefulset_container_count > 0:
                deployment_output.append("\t\t\t\t")
            statefulset_container_count += 1
            
        
    ## Get Daemonset
    v1_daemonsets = list()
    
    if OCP_VERSION == "3":
        v1_daemonsets = dyn_client.resources.get(api_version='extensions/v1beta1', kind='DaemonSet')
    else:
        v1_daemonsets = dyn_client.resources.get(api_version='apps/v1', kind='DaemonSet')
        
    daemonset_list = v1_daemonsets.get(namespace=project.metadata.name)

    for daemonset in daemonset_list.items:
        strategytype = NoneToBlank(daemonset.spec.updateStrategy.get("type"))
            
        deployment_output.append("Daemonset" + "\t" + daemonset.metadata.name + "\t" + strategytype + "\t" + "N/A" + "\t")

        daemonset_container_count = 0
        for daemonset_container in daemonset.spec.template.spec.containers:
            limits = daemonset_container.resources.get("limits")
            requests = daemonset_container.resources.get("requests")
            if limits is not None:
                limitscpu = NoneToZero(limits.get("cpu"))
                limitsmemory = NoneToZero(limits.get("memory"))
            else:
                limitscpu = "0"
                limitsmemory = "0"
                
            if requests is not None:
                requestscpu = NoneToZero(requests.get("cpu"))
                requestsmemory = NoneToZero(requests.get("memory"))
            else:
                requestscpu = "0"
                requestsmemory = "0"
            
            deployment_container_output.append(daemonset_container.name + "\t" + MeasureToNum(requestscpu) + "\t" + MeasureToNum(limitscpu) + "\t" + MeasureToNum(requestsmemory) + "\t" + MeasureToNum(limitsmemory) + "\t")
            if daemonset_container_count > 0:
                deployment_output.append("\t\t\t\t")
            daemonset_container_count += 1
            
    ## Get LimitRange
    v1_limitranges = dyn_client.resources.get(api_version='v1', kind='LimitRange')
    limitrange_list = v1_limitranges.get(namespace=project.metadata.name)

    limitrange_output = list()
    
    for limitrange in limitrange_list.items:
        pod_limitrange_max_cpu = "0"
        pod_limitrange_max_memory = "0"
        pod_limitrange_min_cpu = "0"
        pod_limitrange_min_memory = "0"
        container_limitrange_max_cpu = "0"
        container_limitrange_max_memory = "0"
        container_limitrange_min_cpu = "0"
        container_limitrange_min_memory = "0"
        container_limitrange_default_cpu = "0"
        container_limitrange_default_memory = "0"
        container_limitrange_defaultrequest_cpu = "0"
        container_limitrange_defaultrequest_memory = "0"
        
        if limitrange.spec.get("limits")[0].type == "Pod":
            pod_limitrange = limitrange.spec.get("limits")[0]
            pod_limitrange_max_cpu = pod_limitrange.max.cpu
            pod_limitrange_max_memory = pod_limitrange.max.memory
            pod_limitrange_min_cpu = pod_limitrange.min.cpu
            pod_limitrange_min_memory = pod_limitrange.min.memory
            if len(limitrange.spec.get("limits")) > 1:
                container_limitrange = limitrange.spec.get("limits")[1]
                container_limitrange_max_cpu = container_limitrange.max.cpu
                container_limitrange_max_memory = container_limitrange.max.memory
                container_limitrange_min_cpu = container_limitrange.min.cpu
                container_limitrange_min_memory = container_limitrange.min.memory
                container_limitrange_default_cpu = container_limitrange.default.cpu
                container_limitrange_default_memory = container_limitrange.default.memory
                container_limitrange_defaultrequest_cpu = container_limitrange.defaultRequest.cpu
                container_limitrange_defaultrequest_memory = container_limitrange.defaultRequest.memory
        else:
            container_limitrange = limitrange.spec.get("limits")[0]
            container_limitrange_max_cpu = container_limitrange.max.cpu
            container_limitrange_max_memory = container_limitrange.max.memory
            container_limitrange_min_cpu = container_limitrange.min.cpu
            container_limitrange_min_memory = container_limitrange.min.memory
            container_limitrange_default_cpu = container_limitrange.default.cpu
            container_limitrange_default_memory = container_limitrange.default.memory
            container_limitrange_defaultrequest_cpu = container_limitrange.defaultRequest.cpu
            container_limitrange_defaultrequest_memory = container_limitrange.defaultRequest.memory
            if len(limitrange.spec.get("limits")) > 1:
                pod_limitrange = limitrange.spec.get("limits")[1]
                pod_limitrange_max_cpu = pod_limitrange.max.cpu
                pod_limitrange_max_memory = pod_limitrange.max.memory
                pod_limitrange_min_cpu = pod_limitrange.min.cpu
                pod_limitrange_min_memory = pod_limitrange.min.memory       

        limitrange_output.append(limitrange.metadata.name + "\t" + MeasureToNum(pod_limitrange_max_cpu) + "\t" + MeasureToNum(pod_limitrange_max_memory) + "\t" + MeasureToNum(pod_limitrange_min_cpu) + "\t" + MeasureToNum(pod_limitrange_min_memory) + "\t" + MeasureToNum(container_limitrange_max_cpu) + "\t" + MeasureToNum(container_limitrange_max_memory) + "\t" + MeasureToNum(container_limitrange_min_cpu) + "\t" + MeasureToNum(container_limitrange_min_memory) + "\t" + MeasureToNum(container_limitrange_default_cpu) + "\t" + MeasureToNum(container_limitrange_default_memory) + "\t" + MeasureToNum(container_limitrange_defaultrequest_cpu) + "\t" + MeasureToNum(container_limitrange_defaultrequest_memory) + "\t")
    
    ## Get Route
    v1_routes = dyn_client.resources.get(api_version='route.openshift.io/v1', kind='Route')
    route_list = v1_routes.get(namespace=project.metadata.name)

    route_output = list()
    
    for route in route_list.items:
        tlstermination = NoneToBlank(route.spec.get("tls.termination"))
        route_output.append(route.metadata.name + "\t" + route.spec.host + "\t" + tlstermination + "\t" + route.spec.to.name + "\t")
        
        
    ## Get Ingress
    v1_ingresses = list()
    
    if OCP_VERSION == "3":
        v1_ingresses = dyn_client.resources.get(api_version='extensions/v1beta1', kind='Ingress')
    else:
        v1_ingresses = dyn_client.resources.get(api_version='config.openshift.io/v1', kind='Ingress')
    
    
    ingress_list = v1_ingresses.get(namespace=project.metadata.name)

    ingress_output = list()
    
    for ingress in ingress_list.items:
        
        ingress_rules_hoststring = ""
        
        if ingress.spec.rules is not None:
            for ingress_rules in ingress.spec.rules:
                ingress_rules_hoststring = ingress_rules_hoststring + "|" + ingress_rules.host
        
        ingress_output.append(ingress.metadata.name + "\t" + ingress_rules_hoststring + "\t")
        
    ## Get ExternalIPs of service
    v1_services = dyn_client.resources.get(api_version='v1', kind='Service')
    
    
    service_list = v1_services.get(namespace=project.metadata.name)

    service_output = list()
    service_externalips_output = list()
    
    for service in service_list.items:
        externalips_list = service.spec.externalIPs
        service_str = service.metadata.name + "\t"
        if externalips_list is not None:
            for externalips in externalips_list:
                service_output.append(service_str + externalips + "\t")
        else:
            service_output.append(service_str + "\t")
        

    ## Get PersistentVolumeClaim & PersistentVolume
    v1_persistentvolumeclaimes = dyn_client.resources.get(api_version='v1', kind='PersistentVolumeClaim')
    
    persistentvolumeclaim_list = v1_persistentvolumeclaimes.get(namespace=project.metadata.name)

    persistentvolumeclaim_output = list()
    persistentvolume_output = list()
    
    for persistentvolumeclaim in persistentvolumeclaim_list.items:
        resources_requests_storage = persistentvolumeclaim.spec.resources.requests.storage
        pvc_status = persistentvolumeclaim.status.phase
        persistentvolumeclaim_output.append(persistentvolumeclaim.metadata.name + "\t" + pvc_status + "\t" + MeasureToNum(resources_requests_storage) + "\t")
        
        persistentvolume = FindPVbyPVC(persistentvolum_list, persistentvolumeclaim.metadata.name)

        if persistentvolume is not None:
            nfs = persistentvolume.spec.get("nfs")
            nfs_server = ""
            nfs_path = ""
            if nfs is not None:
                nfs_server = nfs.server
                nfs_path = nfs.path
            
            if persistentvolume.metadata.annotations is not None:
                persistentvolume_output.append(persistentvolume.metadata.name + "\t" + NoneToBlank(persistentvolume.metadata.annotations.get("volume.beta.kubernetes.io/mount-options")) + "\t" + nfs_server + "\t" + nfs_path + "\t" + persistentvolume.spec.persistentVolumeReclaimPolicy + "\t")
            else:
                persistentvolume_output.append(persistentvolume.metadata.name + "\t\t" + nfs_server + "\t" + nfs_path + "\t" + persistentvolume.spec.persistentVolumeReclaimPolicy + "\t")
        else:
            persistentvolume_output.append("\t\t\t\t\t")
        
        
    ## additional records add to table        
    loop = [len(resourcequota_output), len(netnamespace_output), len(deployment_container_output), len(limitrange_output), len(route_output), len(ingress_output), len(service_output), len(persistentvolumeclaim_output)]

    for x in range(0, max(loop)):
        if len(project_output) <= x:
            print("\t\t\t", end='')
        else:
            print(project_output[x], end='')
 
        if len(netnamespace_output) <= x:
            print("\t\t", end='')
        else:
            print(netnamespace_output[x], end='')           
        
        if len(resourcequota_output) <= x:
            print("\t\t\t\t\t", end='')   
        else:
            print(resourcequota_output[x], end='')
            
        if len(deployment_output) <= x:
            print("\t\t\t\t", end='')
        else:
            print(deployment_output[x], end='')
        
        if len(deployment_container_output) <= x:
            print("\t\t\t\t\t", end='')
        else:
            print(deployment_container_output[x], end='')
            
        if len(limitrange_output) <= x:
            print("\t\t\t\t\t\t\t\t\t\t\t\t\t", end='')
        else:
            print(limitrange_output[x], end='')
       
        if len(route_output) <= x:
            print("\t\t\t\t", end='')
        else:
            print(route_output[x], end='')
            
        if len(ingress_output) <= x:
            print("\t\t", end='')
        else:
            print(ingress_output[x], end='')
            
        if len(service_output) <= x:
            print("\t\t", end='')
        else:
            print(service_output[x], end='')       
            
        if len(persistentvolumeclaim_output) <= x:
            print("\t\t\t", end='')
        else:
            print(persistentvolumeclaim_output[x], end='')    
            
        if len(persistentvolume_output) <= x:
            print("\t\t\t\t\t", end='')
        else:
            print(persistentvolume_output[x], end='')    
                     
        print("")
    time.sleep(1)
