from kubernetes import client, config
from openshift.dynamic import DynamicClient

import warnings
import time
import sys

from com.lguplus.common import *

OCP_VERSION = sys.argv[1]

warnings.filterwarnings("ignore")

k8s_client = config.new_client_from_config()
dyn_client = DynamicClient(k8s_client)

loop = []


## Users
v1_users = dyn_client.resources.get(api_version='user.openshift.io/v1', kind='User')
user_list = v1_users.get()
        
## Retreive Data from K8S for each project
v1_projects = dyn_client.resources.get(api_version='project.openshift.io/v1', kind='Project')
project_list = v1_projects.get()

    
## Report Header
print("SCOPE\tUSERNAME\tIM_PROVIDER\tGROUP\tROLE\tCREATIONTIME")

## Cluster-wide rolebinding

clusterrole_output = list()

    
project_output = list()
    
project_output.append("cluster-wide-role\t")
    
v1_clusterrolebindings = dyn_client.resources.get(api_version='rbac.authorization.k8s.io/v1', kind='ClusterRoleBinding')
clusterrolebinding_list = v1_clusterrolebindings.get()
    
user_output = list()
clusterrolebinding_output = list()
user_identity_output = list()
group_output = list()
creationtime_output = list()
    
for clusterrolebinding in clusterrolebinding_list.items:
    if clusterrolebinding.roleRef.name not in MANAGED_CLUSTER_ROLES:
        continue
    
    clusterrolebinding_array = clusterrolebinding.subjects
    if clusterrolebinding_array is not None:
        for clusterrolebinding_idx in range(0, len(clusterrolebinding_array)):
            if clusterrolebinding_array[clusterrolebinding_idx].kind == "User":
                user_output.append(clusterrolebinding_array[clusterrolebinding_idx].name + "\t")
                clusterrolebinding_output.append(clusterrolebinding.roleRef.name + "\t")
                creationtime_output.append(clusterrolebinding.metadata.creationTimestamp + "\t")
                user = GetUserByName(user_list, clusterrolebinding_array[clusterrolebinding_idx].name)
                if user is not None:
                    group_output.append(NoneToBlank(user.groups) + "\t")
                    user_identity_output.append(user.identities[0] + "\t")
                else:
                    group_output.append("\t")
                    user_identity_output.append("\t")                

loop = [len(project_output), len(user_output), len(group_output), len(clusterrolebinding_output), len(user_identity_output)]

for x in range(0, max(loop)):
    if len(project_output) <= x:
        print("\t", end='')
    else:
        print(project_output[x], end='')
 
    if len(user_output) <= x:
        print("\t", end='')
    else:
        print(user_output[x], end='')           
        
    if len(user_identity_output) <= x:
        print("\t", end='')   
    else:
        print(user_identity_output[x], end='')
            
    if len(group_output) <= x:
        print("\t", end='')
    else:
        print(group_output[x], end='')
        
    if len(clusterrolebinding_output) <= x:
        print("\t", end='')
    else:
        print(clusterrolebinding_output[x], end='')

    if len(creationtime_output) <= x:
        print("\t", end='')
    else:
        print(creationtime_output[x], end='')            
   
    print("")   
    
loop = []

## Project scope rolebinding

for project in project_list.items:
    
    if project.metadata.name in EXCEPT_PROJECT:
        continue
    
    project_output = list()
    
    project_output.append(project.metadata.name + "\t")
    
    v1_rolebindings = dyn_client.resources.get(api_version='rbac.authorization.k8s.io/v1', kind='RoleBinding')
    rolebinding_list = v1_rolebindings.get(namespace=project.metadata.name)
    user_output = list()
    rolebinding_output = list()
    user_identity_output = list()
    group_output = list()
    creationtime_output = list()
    
    for rolebinding in rolebinding_list.items:
        rolebinding_array = rolebinding.subjects
        if rolebinding_array is not None:
            for rolebinding_idx in range(0, len(rolebinding_array)):
                if rolebinding_array[rolebinding_idx].kind == "User":
                    user_output.append(rolebinding_array[rolebinding_idx].name + "\t")
                    rolebinding_output.append(rolebinding.roleRef.name + "\t")
                    creationtime_output.append(rolebinding.metadata.creationTimestamp + "\t")
                    user = GetUserByName(user_list, rolebinding_array[rolebinding_idx].name)
                    if user is not None:
                        group_output.append(NoneToBlank(user.groups) + "\t")
                        user_identity_output.append(user.identities[0] + "\t")
                    else:
                        group_output.append("\t")
                        user_identity_output.append("\t")
            
                                            
    ## additional records add to table        
    loop = [len(project_output), len(user_output), len(group_output), len(rolebinding_output), len(user_identity_output)]

    for x in range(0, max(loop)):
        if len(project_output) <= x:
            print("\t", end='')
        else:
            print(project_output[x], end='')
 
        if len(user_output) <= x:
            print("\t", end='')
        else:
            print(user_output[x], end='')           
        
        if len(user_identity_output) <= x:
            print("\t", end='')   
        else:
            print(user_identity_output[x], end='')
            
        if len(group_output) <= x:
            print("\t", end='')
        else:
            print(group_output[x], end='')
        
        if len(rolebinding_output) <= x:
            print("\t", end='')
        else:
            print(rolebinding_output[x], end='')

        if len(creationtime_output) <= x:
            print("\t", end='')
        else:
            print(creationtime_output[x], end='')            
   
        print("")
    time.sleep(1)
