import os 
from flask import Flask, request, url_for, render_template, session
from com.lguplus.nodehandler import *
from com.lguplus.node import *
from com.lguplus.project import *
from com.lguplus.deploymenthandler import *
from com.lguplus.networkhandler import *
from com.lguplus.storagehandler import *

app = Flask(__name__)
app.secret_key = "1234!"

@app.route("/node_report", methods=['POST'])
def node_report():
    cluster_name = request.form["cluster_name"]
    ocp_ver = request.form["ocp_ver"]
    username = ""
    password = ""
    if "username" in session:
        username = session["username"]
        password = session["password"]
    else:
        username = request.form["username"]
        password = request.form["password"]
        session["username"] = username
        session["password"] = password

    ## add comment - 24/01/16

    node_list = GetNodeList(cluster_name, username, password, ocp_ver)
    return render_template('nodereport.html', title = "노드 리포트", result = node_list, current_cluster = cluster_name, current_report = "node", current_ocp_ver = ocp_ver)

@app.route("/deployment_report", methods=['POST'])
def deployment_report():
    cluster_name = request.form["cluster_name"]
    ocp_ver = request.form["ocp_ver"]
    username = ""
    password = ""
    if "username" in session:
        username = session["username"]
        password = session["password"]
    else:
        username = request.form["username"]
        password = request.form["password"]
        session["username"] = username
        session["password"] = password

    deploy_list = GetDeployList(cluster_name, username, password)
    return render_template("deploymentreport.html", title = "배포 리포트", result = deploy_list, current_cluster = cluster_name, current_report = "deployment", current_ocp_ver = ocp_ver)

@app.route("/storage_report", methods=['POST'])
def storage_report():
    cluster_name = request.form["cluster_name"]
    ocp_ver = request.form["ocp_ver"]
    username = ""
    password = ""
    if "username" in session:
        username = session["username"]
        password = session["password"]
    else:
        username = request.form["username"]
        password = request.form["password"]
        session["username"] = username
        session["password"] = password

    storage_obj = request.form["storage_obj"]
    render_page = ""
    title_str = ""
    storage_list = list()
    if storage_obj == "pv" :
        storage_list = GetPVList(cluster_name, username, password)
        render_page = "storagereport_pv.html" 
        title_str = "퍼시스턴트 볼륨 리포트"
    elif storage_obj == "pvc" :
        storage_list = GetPVCList(cluster_name, username, password)
        render_page = "storagereport_pvc.html"
        title_str = "퍼시스턴트 볼륨 클레임 리포트"

    return render_template(render_page, title = title_str, result = storage_list, current_cluster = cluster_name, current_report = "storage_report_" + storage_obj, current_ocp_ver = ocp_ver)

@app.route("/project_report", methods=['POST'])
def project_report():
    cluster_name = request.form["cluster_name"]
    ocp_ver = request.form["ocp_ver"]
    username = ""
    password = ""
    if "username" in session:
        username = session["username"]
        password = session["password"]
    else:
        username = request.form["username"]
        password = request.form["password"]
        session["username"] = username
        session["password"] = password

    deploy_list = GetDeployList(cluster_name, username, password)
    return render_template("projectreport.html", title = "프로젝트 리포트", result = deploy_list, current_cluster = cluster_name, current_report = "project", current_ocp_ver = ocp_ver)

@app.route("/network_report", methods=['POST'])
def network_report():
    cluster_name = request.form["cluster_name"]
    ocp_ver = request.form["ocp_ver"]
    username = ""
    password = ""
    if "username" in session:
        username = session["username"]
        password = session["password"]
    else:
        username = request.form["username"]
        password = request.form["password"]
        session["username"] = username
        session["password"] = password
        
    network_list = GetNetworkList(cluster_name, username, password)
    return render_template("networkreport.html", title = "네트워크 리포트", result = network_list, current_cluster = cluster_name, current_report = "network", current_ocp_ver = ocp_ver)



@app.route("/", methods=['GET'])
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)