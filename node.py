from kubernetes import client, config
from openshift.dynamic import DynamicClient

import warnings
import time
import sys

from com.lguplus.common import *


OCP_VERSION = sys.argv[1]

warnings.filterwarnings("ignore")

k8s_client = config.new_client_from_config()
dyn_client = DynamicClient(k8s_client)

loop = []


    
## Report Header
print("NODE\t\t\tCAPACITY\t\tREQEUSTS\t\t\t\tLIMITS\t\t\t\tCONDITION\t\t\t\t")
print("NAME\tIP\tNODEROLE\tCore\tMemory\tCore\tMemory\tCore(%)\tMemory(%)\tCore\tMemory\tCore(%)\tMemory(%)\tREADY\tMEMORY_PRESSURE\tDISK_PRESSURE\tPID_PRESSURE\t")

## Get Node List
v1_nodes = dyn_client.resources.get(api_version='v1', kind='Node')
node_list = v1_nodes.get()

v1_pods = dyn_client.resources.get(api_version='v1', kind='Pod')
pod_list = v1_pods.get()

for node in node_list.items:
    
    node_output = list()
    
    node_output.append(node.metadata.name + "\t")
    
    ip_list = node.status.get("addresses")
    ip_output = list()
    for x in range(0, len(ip_list)):
        if ip_list[x].type == 'InternalIP':
            ip_output.append(ip_list[x].address + "\t")
    
    capacity = node.status.get("allocatable")
    capacity_output = list()
    node_capacity_cpu = CPUMeasureToNum(capacity.cpu)
    node_capacity_memory = MemoryMeasureToNum(capacity.memory)
    capacity_output.append(node_capacity_cpu + "\t" + node_capacity_memory + "\t")
    
    
    allocated_output = list()

    
    cpu_requests_sum = 0.0
    memory_requests_sum = 0.0
    
    ## Get Allocated pod aggregation on this node
    
    for pod in pod_list.items:
        if pod.spec.nodeName == node.metadata.name and pod.status.phase == "Running":
            containers = pod.spec.containers
            for x in range(0, len(containers)):
                if containers[x].resources is not None and containers[x].resources.requests is not None:
                    cpu_requests_sum += float(CPUMeasureToNum(containers[x].resources.requests.cpu))
                    memory_requests_sum += float(MemoryMeasureToNum(containers[x].resources.requests.memory))
    
    allocated_output.append(str(round(cpu_requests_sum, 2)) + "\t" + str(round(memory_requests_sum, 2)) + "\t")
    
    allocated_perc_output = list()
    allocated_perc_output.append(str(round(cpu_requests_sum/float(node_capacity_cpu)*100.0, 2)) + "\t" + str(round(memory_requests_sum/float(node_capacity_memory)*100.0, 2)) + "\t")
    
    
    limits_output = list()
    
    cpu_limits_sum = 0.0
    memory_limits_sum = 0.0
    
    ## Get Limits pod aggregation on this node
    
    for pod in pod_list.items:
        if pod.spec.nodeName == node.metadata.name and pod.status.phase == "Running":
            containers = pod.spec.containers
            for x in range(0, len(containers)):
                if containers[x].resources is not None and containers[x].resources.limits is not None:
                    cpu_limits_sum += float(CPUMeasureToNum(containers[x].resources.limits.cpu))
                    memory_limits_sum += float(MemoryMeasureToNum(containers[x].resources.limits.memory))
    
    limits_output.append(str(round(cpu_limits_sum, 2)) + "\t" + str(round(memory_limits_sum, 2)) + "\t")   
    
    limits_perc_output = list()
    limits_perc_output.append(str(round(cpu_limits_sum/float(node_capacity_cpu)*100.0, 2)) + "\t" + str(round(memory_limits_sum/float(node_capacity_memory)*100.0, 2)) + "\t")


    condition_output = list()
    condition_memorypressure = ""
    condition_diskpressure = ""
    condition_ready = ""
    condition_pidpressure = ""
    
    
    condition_list = node.status.get("conditions")
    for x in range(0, len(condition_list)):
        if condition_list[x].type == "MemoryPressure":
            condition_memorypressure = condition_list[x].status
        if condition_list[x].type == "DiskPressure":
            condition_diskpressure = condition_list[x].status
        if condition_list[x].type == "PIDPressure":
            condition_pidpressure = condition_list[x].status
        if condition_list[x].type == "Ready":
            condition_ready = condition_list[x].status
    
    
    condition_output.append(condition_ready + "\t" + condition_memorypressure + "\t" + condition_diskpressure + "\t" + condition_pidpressure + "\t")
    
    role_output = list()
    
    workerrole = node.metadata.labels.get("node-role.kubernetes.io/worker")
    masterrole = node.metadata.labels.get("node-role.kubernetes.io/master")
    infrarole = node.metadata.labels.get("node-role.kubernetes.io/infra")
    routerrole = node.metadata.labels.get("node-role.kubernetes.io/router")
    
    if workerrole is not None:
        role_output.append("node-role.kubernetes.io/worker\t")
        
    if masterrole is not None:
        role_output.append("node-role.kubernetes.io/master\t")
            
    if infrarole is not None:
        role_output.append("node-role.kubernetes.io/infra\t")
        
    if routerrole is not None:
        role_output.append("node-role.kubernetes.io/router\t")


    ## additional records add to table        
    loop = [len(node_output), len(ip_output), len(capacity_output), len(allocated_output), len(limits_output), len(condition_output), len(role_output)]

    for x in range(0, max(loop)):
        if len(node_output) <= x:
            print("\t", end='')
        else:
            print(node_output[x], end='')
 
        if len(ip_output) <= x:
            print("\t", end='')
        else:
            print(ip_output[x], end='')           

        if len(role_output) <= x:
            print("\t", end='')
        else:
            print(role_output[x], end='')     
                    
        if len(capacity_output) <= x:
            print("\t\t", end='')   
        else:
            print(capacity_output[x], end='')
            
        if len(allocated_output) <= x:
            print("\t\t", end='')
        else:
            print(allocated_output[x], end='')

        if len(allocated_perc_output) <= x:
            print("\t\t", end='')
        else:
            print(allocated_perc_output[x], end='')
                    
        if len(limits_output) <= x:
            print("\t\t", end='')
        else:
            print(limits_output[x], end='')
 
        if len(limits_perc_output) <= x:
            print("\t\t", end='')
        else:
            print(limits_perc_output[x], end='')
                       
        if len(condition_output) <= x:
            print("\t\t\t\t", end='')
        else:
            print(condition_output[x], end='')

       
   
        print("")
    time.sleep(1)
