from com.lguplus.common import *

class Ingress:

    dyn_client = None
    cluster_name = ""
    name = ""
    rules = list()
    tls = list()
    to_service = list()
    to_host = list()
    creationtime = ""
    OCP_VERSION = "4"
    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.rules, end="\n")
        print(self.tls, end="\n")
        print(self.to_service, end="\n")
        print(self.to_host, end="\n")
        print(self.creationtime, end="\n")

        return


    def report(self, ingress):
        self.name = ingress.metadata.name
        ing_rules = ingress.spec.get("rules")
        for i in range(0, len(ing_rules)):
            rule = ing_rules[i]
            paths = rule.get("http").get("paths")
            self.to_service = list()
            self.to_host = list()
            for p in range(0, len(paths)):
                path = paths[p]
                svc = path.get("backend").get("service")
                self.to_service.append(svc.get("name") + "/" + str(svc.get("port").get("number")))
                self.to_host.append(str(rule.get("host")) + str(path.get("path")))
        
        ##ing_hosts = ingress.spec.get("tls.hosts")
        ##for i in range (0, len(ing_hosts)):
        ##    tls = ing_hosts[i]
        ##    self.tls.append(tls.get("secretName"))

        self.creationtime = ingress.metadata.get("creationTimestamp")

        ## self.print()

        return self
