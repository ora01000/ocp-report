from com.lguplus.common import *

class Service:

    dyn_client = None
    name = ""
    clusterips = list()
    externalips = list()
    type = ""
    ports = list()
    creationtime = ""
    OCP_VERSION = "4"
    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.clusterips, end="\n")
        print(self.externalips, end="\n")
        print(self.type, end="\n")
        print(self.ports, end="\n")
        print(self.creationtime, end="\n")
       
        return

    def report(self, service):
        self.name = service.metadata.name
        self.clusterips = service.spec.clusterIPs
        self.type = service.spec.type
        if self.type == "ExternalIP":
            self.externalips = service.spec.exteralIPs

        self.ports = list()

        for svc in service.spec.ports:
            svc_port = svc.get("port")
            svc_protocol = svc.get("protocol")
            svc_target_port = svc.get("targetPort")
            self.ports.append(str(svc_port) + "/" + svc_protocol + "->" + str(svc_target_port))
        
        self.creationtime = service.metadata.get("creationTimestamp")

        ##self.print()

        return self
