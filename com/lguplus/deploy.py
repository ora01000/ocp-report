from com.lguplus.common import *
from com.lguplus.container import *

class Deploy:

    cluster_name = ""
    project_name = ""
    name = ""
    type = ""
    updatestrategy = ""
    replica = "0"
    container_list = list()
    rowspan_str = ""
    rowspan_cnt = 0
    
    def __init__(self, cluster_name, project_name, type):
        self.cluster_name = cluster_name
        self.project_name = project_name
        self.type = type

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.project_name, end="\n")
        print(self.name, end="\n")
        print(self.type, end="\n")
        print(self.updatestrategy, end="\n")
        print(self.replica, end="\n")
        return

    def getRowSpanStr(self):
        cnt = 0
        for cont in self.container_list:
            cnt = cnt + 1
        if cnt != 0:
            self.rowspan_str = "rowspan=" + str(cnt)
            self.rowspan_cnt = cnt
        return
    
    def report(self, deploy):

        self.name = deploy.metadata.name
        self.container_list = list()

        if self.type == "Statefulset" or self.type == "Daemonset":
            self.updatestrategy = NoneToBlank(deploy.spec.updateStrategy.get("type"))
        else:
            self.updatestrategy = NoneToBlank(deploy.spec.strategy.get("type"))
        self.replica = str(deploy.spec.replicas)
        
        container = Container(deploy)
        self.container_list.append(container.report())
                   
        ##self.print()
        self.getRowSpanStr()

        return self

