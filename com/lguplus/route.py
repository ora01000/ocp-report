from com.lguplus.common import *

class Route:

    dyn_client = None
    name = ""
    host = ""
    to_name = ""
    tls_termination = ""
    creationtime = ""
    OCP_VERSION = "4"
    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.host, end="\n")
        print(self.creationtime, end="\n")
        print(self.to_name, end="\n")
        print(self.tls_termination, end="\n")
       
        return

    def report(self, route):
        self.name = route.metadata.name
        self.host = route.spec.host
        self.to_name = route.spec.to.name
        self.tls_termination = NoneToBlank(route.spec.get("tls.termination"))
        self.creationtime = route.metadata.get("creationTimestamp")

        ##self.print()

        return self
