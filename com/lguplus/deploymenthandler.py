from kubernetes import client, config
from openshift.dynamic import DynamicClient
from openshift.helper.userpassauth import OCPLoginConfiguration

import warnings

from com.lguplus.common import *
from com.lguplus.node import *
from com.lguplus.project import *
from com.lguplus.network import *


global kubeConfig

def GetDeployList(cluster_name, usn, pwd):

    kubeConfig = GetConnection(apihost="https://api." + cluster_name + ".lguplus.co.kr:6443", username=usn, password=pwd)

    warnings.filterwarnings("ignore")

    result = list()

    k8s_client = client.ApiClient(kubeConfig)
    dyn_client = DynamicClient(k8s_client)

    # Get Hostsubnet
    v1_hostsubnets = dyn_client.resources.get(api_version='network.openshift.io/v1', kind='HostSubnet')
    hostsubnet_list = v1_hostsubnets.get()

    # Get NetNamespace
    v1_netnamespaces = dyn_client.resources.get(api_version='network.openshift.io/v1', kind='NetNamespace')
    netnamespace_list = v1_netnamespaces.get()

    ## Retreive Data from K8S for each project
    v1_projects = dyn_client.resources.get(api_version='project.openshift.io/v1', kind='Project')
    project_list = v1_projects.get()

    for project in project_list.items:
        
        if project.metadata.name in EXCEPT_PROJECT:
            continue

        pj = Project(cluster_name, dyn_client)
        result.append(pj.report(project, netnamespace_list, hostsubnet_list))
        
    return result
