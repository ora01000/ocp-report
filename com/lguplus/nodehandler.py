from kubernetes import client, config
from openshift.dynamic import DynamicClient
from openshift.helper.userpassauth import OCPLoginConfiguration

import warnings
import time
import sys

from com.lguplus.common import *
from com.lguplus.node import *

global kubeConfig

def GetNodeList(cluster_name, usn, pwd, ver):

    print(cluster_name)
    print(ver)
    
    if ver == "3":
        kubeConfig = GetConnection(apihost="https://" + cluster_name + ":8443", username=usn, password=pwd, version=ver)
    else: 
        kubeConfig = GetConnection(apihost="https://api." + cluster_name + ".lguplus.co.kr:6443", username=usn, password=pwd, version=ver)

    warnings.filterwarnings("ignore")

    node_report_items = list()

    k8s_client = client.ApiClient(kubeConfig)
    dyn_client = DynamicClient(k8s_client)

    ## Get Node List
    v1_nodes = dyn_client.resources.get(api_version='v1', kind='Node')
    node_list = v1_nodes.get()

    v1_pods = dyn_client.resources.get(api_version='v1', kind='Pod')

    for node in node_list.items:
        node_selector = "spec.nodeName=" + node.metadata.name
        pod_list = v1_pods.get(field_selector=node_selector)
        nodeObject = Node
        nodeObject = Node(cluster_name).report(node, pod_list)
        node_report_items.append(nodeObject)
        
    return node_report_items
