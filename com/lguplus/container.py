from com.lguplus.common import *

class Container:

    deploy = None
    name = ""
    cpu_requests = "0"
    cpu_limits = "0"
    memory_requests = "0"
    memory_limits = "0"

    def __init__(self, deploy):
        self.deploy = deploy

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.project_name, end="\n")
        print(self.deploy_name, end="\n")
        print(self.name, end="\n")
        print(self.cpu_requests, end="\n")
        print(self.cpu_limits, end="\n")
        print(self.memory_requests, end="\n")
        print(self.memory_limits, end="\n")
        return
    
    def report(self):

        limitscpu = "0"
        limitsmemory = "0"
        requestscpu = "0"
        requestsmemory = "0"

        for deployment_container in self.deploy.spec.template.spec.containers:
            limits = deployment_container.resources.get("limits")
            requests = deployment_container.resources.get("requests")
            if limits is not None:
                limitscpu = NoneToZero(limits.get("cpu"))
                limitsmemory = NoneToZero(limits.get("memory"))
            else:
                limitscpu = "0"
                limitsmemory = "0"
                
            if requests is not None:
                requestscpu = NoneToZero(requests.get("cpu"))
                requestsmemory = NoneToZero(requests.get("memory"))
            else:
                requestscpu = "0"
                requestsmemory = "0"

        self.name = deployment_container.name
        self.cpu_requests = CPUMeasureToNum(requestscpu)
        self.cpu_limits = CPUMeasureToNum(limitscpu)
        self.memory_requests = MemoryMeasureToNum(requestsmemory)
        self.memory_limits = MemoryMeasureToNum(limitsmemory) 
   
        ##self.print()

        return self

