from opcode import stack_effect
from kubernetes import client, config
from openshift.dynamic import DynamicClient
from openshift.helper.userpassauth import OCPLoginConfiguration

import warnings

from com.lguplus.common import *
from com.lguplus.node import *
from com.lguplus.project import *
from com.lguplus.persistentvolume import *
from com.lguplus.persistentvolumeclaim import *


global kubeConfig

def GetPVList(cluster_name, usn, pwd):

    kubeConfig = GetConnection(apihost="https://api." + cluster_name + ".lguplus.co.kr:6443", username=usn, password=pwd)

    warnings.filterwarnings("ignore")

    result = list()

    k8s_client = client.ApiClient(kubeConfig)
    dyn_client = DynamicClient(k8s_client)

    ## Retreive cluster-wide PV from K8S
    v1_pvs = dyn_client.resources.get(api_version='v1', kind='PersistentVolume')
    pv_list = v1_pvs.get()

    for pv in pv_list.items:

        pvs = PersistentVolume(cluster_name, dyn_client)
        result.append(pvs.report(pv))

    return result


def GetPVCList(cluster_name, usn, pwd):

    kubeConfig = GetConnection(apihost="https://api." + cluster_name + ".lguplus.co.kr:6443", username=usn, password=pwd)

    warnings.filterwarnings("ignore")

    result = list()

    k8s_client = client.ApiClient(kubeConfig)
    dyn_client = DynamicClient(k8s_client)

    ## Retreive Data from K8S for each project
    v1_projects = dyn_client.resources.get(api_version='project.openshift.io/v1', kind='Project')
    project_list = v1_projects.get()

    v1_persistentvolumeclaims = dyn_client.resources.get(api_version='v1', kind='PersistentVolumeClaim')

    for project in project_list.items:
        
        if project.metadata.name in EXCEPT_PROJECT:
            continue

        pvc_list = v1_persistentvolumeclaims.get(namespace=project.metadata.name)

        for persistentvolumeclaim in pvc_list.items:
            pvc = PersistentVolumeClaim(cluster_name, project.metadata.name, dyn_client)
            result.append(pvc.report(persistentvolumeclaim))

    return result