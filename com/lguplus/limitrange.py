import warnings
import time
import sys

from com.lguplus.common import *

class LimitRange:

    cluster_name = ""
    project_name = ""
    name = ""
    pod_max_cpu = "0"
    pod_max_memory = "0"
    pod_min_cpu = "0"
    pod_min_memory = "0"
    container_max_cpu = "0"
    container_max_memory = "0"
    container_min_cpu = "0"
    container_min_memory = "0"
    default_cpu = "0"
    default_memory = "0"
    default_request_cpu = "0"
    default_request_memory = "0"
    
    def __init__(self, cluster_name, project_name):
        self.cluster_name = cluster_name
        self.project_name = project_name

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.project_name, end="\n")
        print(self.name, end="\n")
        print(self.pod_max_cpu, end="\n")
        print(self.pod_max_memory, end="\n")
        print(self.pod_min_cpu, end="\n")
        print(self.pod_min_memory, end="\n")
        print(self.container_max_cpu, end="\n")
        print(self.container_max_memory, end="\n")
        print(self.container_min_cpu, end="\n")
        print(self.container_min_memory, end="\n")
        print(self.default_cpu, end="\n")
        print(self.default_memory, end="\n")
        print(self.default_request_cpu, end="\n")
        print(self.default_request_memory, end="\n")
        return
    
    def report(self, limitrange):
        pod_limitrange_max_cpu = "0"
        pod_limitrange_max_memory = "0"
        pod_limitrange_min_cpu = "0"
        pod_limitrange_min_memory = "0"
        container_limitrange_max_cpu = "0"
        container_limitrange_max_memory = "0"
        container_limitrange_min_cpu = "0"
        container_limitrange_min_memory = "0"
        container_limitrange_default_cpu = "0"
        container_limitrange_default_memory = "0"
        container_limitrange_defaultrequest_cpu = "0"
        container_limitrange_defaultrequest_memory = "0"

        if limitrange.spec.get("limits")[0].type == "Pod":
            pod_limitrange = limitrange.spec.get("limits")[0]
            pod_limitrange_max_cpu = pod_limitrange.max.cpu
            pod_limitrange_max_memory = pod_limitrange.max.memory
            pod_limitrange_min_cpu = pod_limitrange.min.cpu
            pod_limitrange_min_memory = pod_limitrange.min.memory
            if len(limitrange.spec.get("limits")) > 1:
                container_limitrange = limitrange.spec.get("limits")[1]
                container_limitrange_max_cpu = container_limitrange.max.cpu
                container_limitrange_max_memory = container_limitrange.max.memory
                container_limitrange_min_cpu = container_limitrange.min.cpu
                container_limitrange_min_memory = container_limitrange.min.memory
                container_limitrange_default_cpu = container_limitrange.default.cpu
                container_limitrange_default_memory = container_limitrange.default.memory
                container_limitrange_defaultrequest_cpu = container_limitrange.defaultRequest.cpu
                container_limitrange_defaultrequest_memory = container_limitrange.defaultRequest.memory
        else:
            container_limitrange = limitrange.spec.get("limits")[0]
            container_limitrange_max_cpu = container_limitrange.max.cpu
            container_limitrange_max_memory = container_limitrange.max.memory
            container_limitrange_min_cpu = container_limitrange.min.cpu
            container_limitrange_min_memory = container_limitrange.min.memory
            container_limitrange_default_cpu = container_limitrange.default.cpu
            container_limitrange_default_memory = container_limitrange.default.memory
            container_limitrange_defaultrequest_cpu = container_limitrange.defaultRequest.cpu
            container_limitrange_defaultrequest_memory = container_limitrange.defaultRequest.memory
            if len(limitrange.spec.get("limits")) > 1:
                pod_limitrange = limitrange.spec.get("limits")[1]
                pod_limitrange_max_cpu = pod_limitrange.max.cpu
                pod_limitrange_max_memory = pod_limitrange.max.memory
                pod_limitrange_min_cpu = pod_limitrange.min.cpu
                pod_limitrange_min_memory = pod_limitrange.min.memory       

        self.name = limitrange.metadata.name
        self.pod_max_cpu = CPUMeasureToNum(pod_limitrange_max_cpu)
        self.pod_max_memory = MemoryMeasureToNum(pod_limitrange_max_memory)
        self.pod_min_cpu = CPUMeasureToNum(pod_limitrange_min_cpu)
        self.pod_min_memory = MemoryMeasureToNum(pod_limitrange_min_memory)
        self.container_max_cpu = CPUMeasureToNum(container_limitrange_max_cpu)
        self.container_max_memory = MemoryMeasureToNum(container_limitrange_max_memory)
        self.container_min_cpu = CPUMeasureToNum(container_limitrange_min_cpu)
        self.container_min_memory = MemoryMeasureToNum(container_limitrange_min_memory)
        self.default_cpu = CPUMeasureToNum(container_limitrange_default_cpu)
        self.default_memory = MemoryMeasureToNum(container_limitrange_default_memory)
        self.default_request_cpu = CPUMeasureToNum(container_limitrange_defaultrequest_cpu)
        self.default_request_memory = MemoryMeasureToNum(container_limitrange_defaultrequest_memory)

        ##self.print()

        return self

