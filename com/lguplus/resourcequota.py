from com.lguplus.common import *

class ResourceQuota:

    cluster_name = ""
    project_name = ""
    name = ""
    cpu_requests = ""
    cpu_limits = ""
    memory_requests = ""
    memory_limits = ""
    used_cpu_requests = ""
    used_cpu_limits = ""
    used_memory_requests = ""
    used_memory_limits = ""
    
    def __init__(self, cluster_name, project_name):
        self.cluster_name = cluster_name
        self.project_name = project_name

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.project_name, end="\n")
        print(self.name, end="\n")
        print(self.cpu_requests, end="\n")
        print(self.cpu_limits, end="\n")
        print(self.memory_requests, end="\n")
        print(self.memory_limits, end="\n")
        print(self.used_cpu_requests, end="\n")
        print(self.used_cpu_limits, end="\n")
        print(self.used_memory_requests, end="\n")
        print(self.used_memory_limits, end="\n")
        return
    
    def report(self, resourcequota):
        
        self.name = resourcequota.metadata.name
        self.cpu_requests = CPUMeasureToNum(resourcequota.spec.hard.get("requests.cpu"))
        self.cpu_limits = CPUMeasureToNum(resourcequota.spec.hard.get("limits.cpu"))
        self.memory_requests = MemoryMeasureToNum(resourcequota.spec.hard.get("requests.memory"))
        self.memory_limits = MemoryMeasureToNum(resourcequota.spec.hard.get("limits.memory"))
        self.used_cpu_requests = CPUMeasureToNum(resourcequota.status.used.get("requests.cpu"))
        self.used_cpu_limits = CPUMeasureToNum(resourcequota.status.used.get("limits.cpu"))
        self.used_memory_requests = MemoryMeasureToNum(resourcequota.status.used.get("requests.memory"))
        self.used_memory_limits = MemoryMeasureToNum(resourcequota.status.used.get("limits.memory"))
       
        ##self.print()

        return self

