from com.lguplus.common import *
from com.lguplus.resourcequota import *
from com.lguplus.limitrange import *
from com.lguplus.deploy import *
from com.lguplus.service import *
from com.lguplus.route import *
from com.lguplus.ingress import *

class Project:

    dyn_client = None
    name = ""
    display_name = ""
    creationtime = ""
    egressip = list()
    hostsubnet = list()
    resourcequota_list = list()
    deployment_list = list()
    limitrange_list = list()
    OCP_VERSION = "4"
    rowspan_str = ""
    rowspan_cnt = 0
    rowspan_prjreport_str = ""
    rowspan_prjreport_cnt = 0

    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.ip_list = list()
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.display_name, end="\n")
        print(self.creationtime, end="\n")
        print(self.egressip, end="\n")
        print(self.hostsubnet, end="\n")
        print(self.resourcequota_list, end="\n")
        print(self.deployment_list, end="\n")
        print(self.limitrange_list, end="\n")
        print(self.rowspan_prjreport_cnt, end="\n")
       
        return

    def getRowSpanStr(self):
        cnt = 0
        for deploy in self.deployment_list:
            for cont in deploy.container_list:
                cnt = cnt + 1
        if cnt != 0:
            self.rowspan_str = "rowspan=" + str(cnt)
            self.rowspan_cnt = cnt
        return

    def getResourceQuotaCnt(self):
        return len(self.resourcequota_list)

    def getLimitragneCnt(self):
        return len(self.limitrange_list)

    def getRowSpanStr4ProjectReport(self):
        resourcequota_cnt = len(self.resourcequota_list)
        limitrange_cnt = len(self.limitrange_list)
        if resourcequota_cnt > 0 or limitrange_cnt > 0:
            self.rowspan_prjreport_str = "rowspan=" + str(max(resourcequota_cnt, limitrange_cnt))
            self.rowspan_prjreport_cnt = max(resourcequota_cnt, limitrange_cnt)
        return
    
    def report(self, project, netnamespace_list, hostsubnet_list):
        self.name = project.metadata.name
        self.display_name = NoneToBlank(project.metadata.annotations.get("openshift.io/display-name"))
        self.creationtime = project.metadata.get("creationTimestamp")

        netnamespace = FindNetNamespaceByName(netnamespace_list, project.metadata.name)
    
        if netnamespace is not None:
            self.egressip = netnamespace.egressIPs
            if self.egressip is None:
                self.egressip = list()
            
            self.hostsubnet = list()
            for egressip in self.egressip:
                self.hostsubnet.append(FindHostsubnetbyEgressIP(hostsubnet_list, egressip)) 

        self.resourcequota_list = self.getResourceQuota()
        self.limitrange_list = self.getLimitRange()
        self.deployment_list = self.getDeploy()

        self.getRowSpanStr()
        self.getRowSpanStr4ProjectReport()

        ##self.print()

        return self

    def getResourceQuota(self):

        result = list()

        ## Get ResourceQuota
        v1_resourcequotas = self.dyn_client.resources.get(api_version='v1', kind='ResourceQuota')
        resourcequota_list = v1_resourcequotas.get(namespace=self.name)

        for resourcequota in resourcequota_list.items:
            rq = ResourceQuota(self.cluster_name, self.name).report(resourcequota)
            result.append(rq)

        return result

    def getLimitRange(self):

        result = list()

        ## Get LimitRange
        v1_limitranges = self.dyn_client.resources.get(api_version='v1', kind='LimitRange')
        limitrange_list = v1_limitranges.get(namespace=self.name)
        
        for limitrange in limitrange_list.items:
            lr = LimitRange(self.cluster_name, self.name).report(limitrange)
            result.append(lr)

        return result


    def getDeploy(self):

        result = list()
                
        ## Get DeploymentConfig
        v1_deploymentconfigs = self.dyn_client.resources.get(api_version='apps.openshift.io/v1', kind='DeploymentConfig')
        deploymentconfig_list = v1_deploymentconfigs.get(namespace=self.name)
        
        for deploymentconfig in deploymentconfig_list.items:
            dc = Deploy(self.cluster_name, self.name, "DeploymentConfig")
            result.append(dc.report(deploymentconfig))
            
        ## Get Deployment
        v1_deployments = list()

        if self.OCP_VERSION == "3":
            v1_deployments = self.dyn_client.resources.get(api_version='extensions/v1beta1', kind='Deployment')
        else:
            v1_deployments = self.dyn_client.resources.get(api_version='apps/v1', kind='Deployment')
        
        deployment_list = v1_deployments.get(namespace=self.name)

        for deployment in deployment_list.items:
            dp = Deploy(self.cluster_name, self.name, "Deployment")
            result.append(dp.report(deployment))
            
        ## Get Statefulset
        v1_statefulsets = self.dyn_client.resources.get(api_version='apps/v1', kind='StatefulSet')
        statefulset_list = v1_statefulsets.get(namespace=self.name)

        for statefulset in statefulset_list.items:
            ss = Deploy(self.cluster_name, self.name, "Statefulset")
            result.append(ss.report(statefulset))
            
        ## Get Daemonset
        v1_daemonsets = list()
        
        if self.OCP_VERSION == "3":
            v1_daemonsets = self.dyn_client.resources.get(api_version='extensions/v1beta1', kind='DaemonSet')
        else:
            v1_daemonsets = self.dyn_client.resources.get(api_version='apps/v1', kind='DaemonSet')
            
        daemonset_list = v1_daemonsets.get(namespace=self.name)

        for daemonset in daemonset_list.items:
            ds = Deploy(self.cluster_name, self.name, "Daemonset")
            result.append(ds.report(daemonset))
            

        return result
