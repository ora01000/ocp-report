from com.lguplus.common import *

class PersistentVolumeClaim:

    cluster_name = ""
    project = ""
    dyn_client = None
    name = ""
    creationtime = ""
    accessmodes = list()
    storage = ""
    phase = ""
    storageclass = ""
    OCP_VERSION = "4"
    
    def __init__(self, cluster_name, project, dyn_client):
        self.cluster_name = cluster_name
        self.project = project
        self.accessmodes = list()
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.creationtime, end="\n")
        print(self.accessmodes, end="\n")
        print(self.storage, end="\n")
        print(self.phase, end="\n")
        print(self.storageclass, end="\n")
       
        return
    
    def report(self, pvc):
        self.name = pvc.metadata.name
        self.creationtime = pvc.metadata.get("creationTimestamp")
        self.accessmodes = pvc.spec.get("accessModes")
        self.storage = pvc.spec.get("resources").get("requests").get("storage")
        self.storageclass = pvc.spec.get("storageClassName")
        self.phase = pvc.status.get("phase")
            
        ## self.print()

        return self

