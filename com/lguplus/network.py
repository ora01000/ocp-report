from com.lguplus.common import *
from com.lguplus.resourcequota import *
from com.lguplus.limitrange import *
from com.lguplus.deploy import *
from com.lguplus.service import *
from com.lguplus.route import *
from com.lguplus.ingress import *

class Network:

    dyn_client = None
    name = ""
    display_name = ""
    creationtime = ""
    route_list = list()
    ingress_list = list()
    service_list = list()

    merged_route_ingress = list()

    OCP_VERSION = "4"
    rowspan_ntwreport_str = ""
    rowspan_ntwreport_cnt = 0

    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.ip_list = list()
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.display_name, end="\n")
        print(self.creationtime, end="\n")
        print(self.service_list, end="\n")
        print(self.route_list, end="\n")
        print(self.ingress_list, end="\n")
        print(self.rowspan_ntwreport_cnt, end="\n")
       
        return

    def getRowSpanStr4NetworkReport(self):
        service_cnt = len(self.service_list)
        route_ingress_cnt = len(self.route_list) + len(self.ingress_list)
        if service_cnt > 0 or route_ingress_cnt > 0:
            self.rowspan_ntwreport_str = "rowspan=" + str(max(service_cnt, route_ingress_cnt))
            self.rowspan_ntwreport_cnt = max(service_cnt, route_ingress_cnt)
        return
    
    def report(self, project):
        self.name = project.metadata.name
        self.display_name = NoneToBlank(project.metadata.annotations.get("openshift.io/display-name"))
        self.creationtime = project.metadata.get("creationTimestamp")
        self.service_list = self.getService()
        self.route_list = self.getRoute()
        self.ingress_list = self.getIngress()

        self.getRowSpanStr4NetworkReport()

        self.mergeRouteNIngress()

        ##self.print()

        return self


    def getService(self):

        result = list()

        ## Get Service
        v1_services = self.dyn_client.resources.get(api_version='v1', kind='Service')
        service_list = v1_services.get(namespace=self.name)

        for service in service_list.items:
            svc = Service(self.cluster_name, self.name).report(service)
            result.append(svc)

        return result

    def getRoute(self):

        result = list()

        ## Get Route
        v1_routes = self.dyn_client.resources.get(api_version='route.openshift.io/v1', kind='Route')
        route_list = v1_routes.get(namespace=self.name)

        for route in route_list.items:
            rt = Route(self.cluster_name, self.name).report(route)
            result.append(rt)

        return result

    def getIngress(self):

        result = list()

        ## Get Ingress
        v1_ingresses = self.dyn_client.resources.get(api_version='networking.k8s.io/v1', kind='Ingress')
        ingress_list = v1_ingresses.get(namespace=self.name)

        for ingress in ingress_list.items:
            ig = Ingress(self.cluster_name, self.name).report(ingress)
            result.append(ig)
        
        return result

    def mergeRouteNIngress(self):
        self.merged_route_ingress = self.route_list + self.ingress_list
        return

    def instanceOf(self, index):
        if isinstance(self.merged_route_ingress[index], Route) == True :
            return "route"
        elif isinstance(self.merged_route_ingress[index], Ingress) == True :
            return "ingress"

    def getServiceCnt(self):
        return len(self.service_list)

    def getRouteIngressCnt(self):
        return len(self.route_list) + len(self.ingress_list)

