from com.lguplus.common import *

class PersistentVolume:

    cluster_name = ""
    dyn_client = None
    name = ""
    creationtime = ""
    accessmodes = list()
    capacity = ""
    claimref_name = ""
    claimref_namespace = ""
    nfs_server = ""
    nfs_path = ""
    reclaimpolicy = ""
    storageclass = ""
    nfsver = ""
    OCP_VERSION = "4"
    
    def __init__(self, cluster_name, dyn_client):
        self.cluster_name = cluster_name
        self.accessmodes = list()
        self.dyn_client = dyn_client

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.name, end="\n")
        print(self.creationtime, end="\n")
        print(self.accessmodes, end="\n")
        print(self.capacity, end="\n")
        print(self.claimref_name, end="\n")
        print(self.claimref_namespace, end="\n")
        print(self.nfs_server, end="\n")
        print(self.nfs_path, end="\n")
        print(self.reclaimpolicy, end="\n")
        print(self.storageclass, end="\n")
        print(self.nfsver, end="\n")

        return
    
    def report(self, pv):
        self.name = pv.metadata.name
        self.creationtime = pv.metadata.get("creationTimestamp")
        self.accessmodes = pv.spec.get("accessModes")
        self.capacity = pv.spec.get("capacity").get("storage")
        if pv.spec.get("claimRef") is not None :
            self.claimref_name = pv.spec.get("claimRef").get("name")
            self.claimref_namespace = pv.spec.get("claimRef").get("namespace")

        if pv.spec.get("nfs") is not None :
            self.nfs_path = pv.spec.get("nfs").get("path")
            self.nfs_server = pv.spec.get("nfs").get("server")
        
        self.reclaimpolicy = pv.spec.get("persistentVolumeReclaimPolicy")
        self.storageclass = pv.spec.get("storageClassName")

        if pv.metadata.get("annotations") is not None :
            if pv.metadata.get("annotations").get("volume.beta.kubernetes.io/mount-options") is not None :
                self.nfsver = pv.metadata.get("annotations").get("volume.beta.kubernetes.io/mount-options")
            
        ## self.print()

        return self

