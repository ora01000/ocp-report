import warnings
import time
import sys

from com.lguplus.common import *

class Node:

    cluster_name = ""
    node_name = ""
    node_capacity_cpu = ""
    node_capacity_memory = ""
    ip_list = None
    cpu_requests_sum = 0.0
    memory_requests_sum = 0.0
    cpu_requests_perc = 0.0
    memory_requests_perc = 0.0
    cpu_limits_sum = 0.0
    memory_limits_sum = 0.0
    cpu_limits_perc = 0.0
    memory_limits_perc = 0.0
    condition_memorypressure = ""
    condition_diskpressure = ""
    condition_ready = ""
    condition_pidpressure = ""
    node_role = ""

    def __init__(self, cluster_name):
        self.cluster_name = cluster_name
        self.ip_list = list()

    def print(self):
        print(self.cluster_name, end="\n")
        print(self.node_name, end="\n")
        print(self.node_capacity_cpu, end="\n")
        print(self.node_capacity_memory, end="\n")
        print(self.ip_list, end="\n")
        print(self.cpu_requests_sum, end="\n")
        print(self.memory_requests_sum, end="\n")
        print(self.cpu_requests_perc, end="\n")
        print(self.memory_requests_perc, end="\n")
        print(self.cpu_limits_sum, end="\n")
        print(self.memory_limits_sum, end="\n")
        print(self.cpu_limits_perc, end="\n")
        print(self.memory_limits_perc, end="\n")
        print(self.condition_memorypressure, end="\n")
        print(self.condition_diskpressure, end="\n")
        print(self.condition_ready, end="\n")
        print(self.condition_pidpressure, end="\n")
        print(self.node_role, end="\n")
        return
    
    def report(self, node, pod_list):
        self.node_name = node.metadata.name
        ip_list = node.status.get("addresses")
        for x in range(0, len(ip_list)):
            if ip_list[x].type == 'InternalIP':
                self.ip_list.append(ip_list[x].address)        

        capacity = node.status.get("allocatable")
        self.node_capacity_cpu = CPUMeasureToNum(capacity.cpu)
        self.node_capacity_memory = MemoryMeasureToNum(capacity.memory)
        
        cpu_requests_sum = 0.0
        memory_requests_sum = 0.0
        cpu_limits_sum = 0.0
        memory_limits_sum = 0.0
        
        ## Get Allocated pod aggregation on this node
        
        for pod in pod_list.items:
            if pod.status.phase == "Running":
                containers = pod.spec.containers
                for x in range(0, len(containers)):
                    if containers[x].resources is not None and containers[x].resources.requests is not None:
                        cpu_requests_sum += float(CPUMeasureToNum(containers[x].resources.requests.cpu))
                        memory_requests_sum += float(MemoryMeasureToNum(containers[x].resources.requests.memory))
                    if containers[x].resources is not None and containers[x].resources.limits is not None:
                        cpu_limits_sum += float(CPUMeasureToNum(containers[x].resources.limits.cpu))
                        memory_limits_sum += float(MemoryMeasureToNum(containers[x].resources.limits.memory))
        
        self.cpu_requests_sum = round(cpu_requests_sum, 2)
        self.memory_requests_sum = round(memory_requests_sum, 2)

        self.cpu_requests_perc = round(cpu_requests_sum/float(self.node_capacity_cpu)*100.0, 2)
        self.memory_requests_perc = round(memory_requests_sum/float(self.node_capacity_memory)*100.0, 2)
        
        self.cpu_limits_sum = round(cpu_limits_sum, 2)
        self.memory_limits_sum = round(memory_limits_sum, 2)
        
        self.cpu_limits_perc = round(cpu_limits_sum/float(self.node_capacity_cpu)*100.0, 2)
        self.memory_limits_perc = round(memory_limits_sum/float(self.node_capacity_memory)*100.0, 2)

        condition_list = node.status.get("conditions")
        for x in range(0, len(condition_list)):
            if condition_list[x].type == "MemoryPressure":
                self.condition_memorypressure = condition_list[x].status
            if condition_list[x].type == "DiskPressure":
                self.condition_diskpressure = condition_list[x].status
            if condition_list[x].type == "PIDPressure":
                self.condition_pidpressure = condition_list[x].status
            if condition_list[x].type == "Ready":
                self.condition_ready = condition_list[x].status

        workerrole = node.metadata.labels.get("node-role.kubernetes.io/worker")
        masterrole = node.metadata.labels.get("node-role.kubernetes.io/master")
        infrarole = node.metadata.labels.get("node-role.kubernetes.io/infra")
        routerrole = node.metadata.labels.get("node-role.kubernetes.io/router")
        
        if workerrole is not None:
            self.node_role = "worker"
            
        if masterrole is not None:
            self.node_role = "master"
                
        if infrarole is not None:
            self.node_role = "infra"

        if routerrole is not None:
            self.node_role = "router"

        ##self.print()

        return self

