from kubernetes import client, config
from openshift.dynamic import DynamicClient
from openshift.helper.userpassauth import OCPLoginConfiguration

import warnings
import time
import sys

OCP_VERSION = "4"

def GetConnection(apihost = "", username = "", password = "", version = "4"):
    
    kubeConfig = OCPLoginConfiguration(ocp_username = username, ocp_password = password)
    kubeConfig.host = apihost
    kubeConfig.verify_ssl = False
    # kubeConfig.ssl_ca_cert = './ocp.pem' # use a certificate bundle for the TLS validation
    
    # Retrieve the auth token
    kubeConfig.get_token()
    
    ## print('Auth token: {0}'.format(kubeConfig.api_key))
    ## print('Token expires: {0}'.format(kubeConfig.api_key_expires))

    OCP_VERSION = version

    return kubeConfig


def NoneToBlank(n):
    if n is None:
        return ""
    return n
   
def NoneToZero(n):
    if n is None:
        return "0"
    return n

def FindPVbyPVC(pvlist, pvcname):
    for pv in pvlist.items:
        if pv.spec.claimRef is not None:
            claimref = pv.spec.claimRef
            if pvcname == claimref.name:
                return pv
    return None

def FindHostsubnetbyEgressIP(hostsubnetlist, egressip):
    for hostsubnet in hostsubnetlist.items:
        egressips_list = hostsubnet.egressIPs
        if egressips_list is not None:
            for egressips in egressips_list:
                if egressips == egressip:
                    return hostsubnet.metadata.name
    return ""

def FindNetNamespaceByName(netnamespacelist, name):
    for netnamespace in netnamespacelist.items:
        if netnamespace.metadata.name == name:
            return netnamespace
    return None

# def MeasureToNum(measure):
#     ret = measure
#     if measure is None:
#         return "0"
#     if measure.endswith("m"):
#         ## CPU millicore
#         ret = measure[:-1] + "/1000"
#         return str(eval(ret))
#     if measure.endswith("Gi"):
#         ## memory GiB
#         return measure[:-2]
#     if measure.endswith("Mi"):
#         ## memory MiB
#         ret = measure[:-2] + "/1000"
#         return str(eval(ret))
#     if measure.endswith("Ki"):
#         ## memory KiB
#         ret = measure[:-2] + "/1000000"
#         return str(eval(ret))
#     if measure.endswith("G"):
#         ## memory G
#         return measure[:-1]
#     if measure.endswith("M"):
#         ## memory M
#         ret = measure[:-1] + "/1000"
#         return str(eval(ret))
#     if measure.endswith("k"):
#         ## memory KiB
#         ret = measure[:-1] + "/1000000"
#         return str(eval(ret))
#     return ret

def MemoryMeasureToNum(measure):
    ret = measure
    if measure is None:
        return "0"
    if measure.endswith("m"):
        ## memory millibyte
        ret = measure[:-1] + "/1000000000"
        return str(eval(ret))
    if measure.endswith("Gi"):
        ## memory GiB
        return measure[:-2]
    if measure.endswith("Mi"):
        ## memory MiB
        ret = measure[:-2] + "/1000"
        return str(eval(ret))
    if measure.endswith("Ki"):
        ## memory KiB
        ret = measure[:-2] + "/1000000"
        return str(eval(ret))
    if measure.endswith("G"):
        ## memory G
        return measure[:-1]
    if measure.endswith("M"):
        ## memory M
        ret = measure[:-1] + "/1000"
        return str(eval(ret))
    if measure.endswith("k"):
        ## memory KiB
        ret = measure[:-1] + "/1000000"
        return str(eval(ret))
    return ret

def CPUMeasureToNum(measure):
    ret = measure
    if measure is None:
        return "0"
    if measure.endswith("m"):
        ## CPU millicore
        ret = measure[:-1] + "/1000"
        return str(eval(ret))
    return ret
    
def GetUserByName(userlist, username):
    for user in userlist.items:
        if user.metadata.name == username:
            return user
    return None


EXCEPT_PROJECT=[ "openshift-console", "openshift-infra", "openshift", "openshift-logging", "openshift-metrics", \
                "openshift-metric-server", "openshift-monitoring", "openshift-node", "openshift-sdn", "openshift-web-console", \
                "management-infra", "kube-system", "kube-public", "istio-system", "istio-operator", "bookinfo", "cicd", \
                "default", "keepalived", "kube-node-lease", "local-storage", "openshift-apiserver", "openshift-apiserver-operator", \
                "openshift-authentication", "openshift-authentication-operator", "openshift-cloud-credential-operator", \
                "openshift-cluster-machine-approver", "openshift-cluster-node-tuning-operator", "openshift-cluster-samples-operator", \
                "openshift-cluster-storage-operator", "openshift-cluster-version", "openshift-config", "openshift-config-managed", \
                "openshift-console-operator", "openshift-controller-manager", "openshift-controller-manager-operator", "openshift-dns", \
                "openshift-dns-operator", "openshift-etcd", "openshift-etcd-operator", "openshift-image-registry", "openshift-ingress", \
                "openshift-ingress-operator", "openshift-insights", "openshift-kni-infra", "openshift-kube-apiserver", "openshift-kube-apiserver-operator", \
                "openshift-kube-controller-manager", "openshift-kube-controller-manager-operator", "openshift-kube-scheduler", "openshift-kube-scheduler-operator", \
                "openshift-kube-storage-version-migrator", "openshift-kube-storage-version-migrator-operator", "openshift-machine-api", "openshift-machine-config-operator", \
                "openshift-marketplace", "openshift-multus", "openshift-network-operator", "openshift-openstack-infra", "openshift-operator-lifecycle-manager", \
                "openshift-operators", "openshift-operators-redhat", "openshift-ovirt-infra", "openshift-pipelines", "openshift-service-ca", \
                "openshift-service-ca-operator", "openshift-service-catalog-apiserver-operator", "openshift-service-catalog-controller-manager-operator", \
                "openshift-user-workload-monitoring", "openshift-vsphere-infra", "kube-dns", "kube-proxy", "openshift-core-operators", "openshift-service-cert-signer", \
                "openshift-cluster-csi-drivers", "openshift-config-operator", "openshift-gitops", "openshift-ingress-canary", "openshift-network-diagnostics", "openshift-oauth-apiserver", \
                "openshift-serverless", "openshift-host-network", "knative-serving", "knative-eventing", "keepalived-operator", "openshift-console-user-settings", "openshift-service-catalog-removed" ]

## Define managed cluster roles
MANAGED_CLUSTER_ROLES = ["cluster-admin", "cluster-reader", "admin", "cru-admin", "view", "edit"]


